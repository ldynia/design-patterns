package best.practice.software.engineering.iinterface;

public interface MessangerInterface {

	public void sendMessage(String receiver, String text);
	
}
