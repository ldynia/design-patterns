package best.practice.software.engineering.iinterface;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		MessangerInterface messanger = new DogMessanger();
//		MessangerInterface messanger = new CatMessanger();
//		MessangerInterface messanger = new PegonMessanger();
		
		messanger.sendMessage("Lukas", "Interface pattern rocks!");
	}

}
