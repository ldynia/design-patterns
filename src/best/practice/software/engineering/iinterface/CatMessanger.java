package best.practice.software.engineering.iinterface;

public class CatMessanger implements MessangerInterface {

	@Override
	public void sendMessage(String receiver, String text) {
		System.out.println("Implementation of CatMessanger");
		System.out.println("Reciver: " + receiver + " \nMessage: " + text);		
	}

}
