package best.practice.software.engineering.delegaton;



/**
 * 
 * @author lukas
 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/delegation.html
 */
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Cat c = new Cat();
		c.makeSound();

		SoundInterface sound = new RoarSound();
		c.setSound(sound);
		c.makeSound();
		
	}

}
