package best.practice.software.engineering.delegaton;

public class Cat {
	
	SoundInterface sound = new MeowSound();

	public void setSound(SoundInterface sound) {
		this.sound = sound;
	}
	
	public void makeSound(){
		this.sound.makeSound();
	}
	
	
}
