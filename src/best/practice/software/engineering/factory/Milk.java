package best.practice.software.engineering.factory;

public class Milk extends Product {

    private float price;

    protected Milk(float price) {
        this.price = price;
    }

    // Implementation
    public float getPrice() {
        return price;
    }

    @Override
    public String getProductType() {
        return "Milk";
    }

}