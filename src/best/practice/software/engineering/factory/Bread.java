package best.practice.software.engineering.factory;

public class Bread extends Product {
	
	private float price;
	
	public Bread(float price) {
		this.price = price;
	}
	
	// Implementation
	public float getPrice() {
		return this.price;
	}
	
	@Override
	public String getProductType() {
        return "Bread";
    }

}
