package best.practice.software.engineering.factory;

public abstract class Product {

    public abstract float getPrice();

    public String getProductType() {
        return "Unknown Product";
    }

}