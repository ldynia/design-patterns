package best.practice.software.engineering.factory;

public class Ananas extends Product {

    private float price;

    protected Ananas(float price) {
        this.price = price;
    }
    
    // Implementation
    public float getPrice() {
        return price;
    }

    @Override
    public String getProductType() {
        return "Ananas";
    }

}