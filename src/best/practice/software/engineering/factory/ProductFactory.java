package best.practice.software.engineering.factory;


/**
 * 
 * @author lukas
 * @comment this is creational pattern
 */
public class ProductFactory {

	public static Product createProduct(String productName) {
		switch(productName){
		    case "Milk" : 	return new Milk(0);
		    case "Bread" :  return new Bread(0);
		    case "Ananas" :  return new Ananas(0);
		    default : return new Bread(0);
		}
	}

}
