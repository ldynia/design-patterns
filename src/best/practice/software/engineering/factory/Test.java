package best.practice.software.engineering.factory;

public class Test {

	/**
	 * @param args
	 * @comment this is creational pattern
	 */
	public static void main(String[] args) {
		// At first we create List of 3 Generic products
        Product[] cart = new Product[3];

        // Populate generic product with concrete type
        cart[0] = ProductFactory.createProduct("Milk");
        cart[1] = ProductFactory.createProduct("Ananas");
        cart[2] = ProductFactory.createProduct("Bread");
        
        System.out.println(cart[0].getProductType());
        System.out.println(cart[1].getProductType());
        System.out.println(cart[2].getProductType());
	}

}
