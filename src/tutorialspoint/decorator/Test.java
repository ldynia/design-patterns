package tutorialspoint.decorator;

public class Test {

	/**
	 * @param args
	 * @link http://www.tutorialspoint.com/design_pattern/decorator_pattern.htm
	 * @comment this is structural pattern
	 */
	public static void main(String[] args) {

		// A Shape
		AbstractShape circle = new Circle();
		circle.draw();
		System.out.print("^^ Normal shape");
		
		// Decorated Shape
		AbstractShape coloredCircle = new ColorShapeDecorator(circle, "Red");
		coloredCircle.draw();
		System.out.print("^^ Decorated shape");
	}

}
