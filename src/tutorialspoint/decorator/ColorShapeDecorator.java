package tutorialspoint.decorator;

public class ColorShapeDecorator extends AbstractShapeDecorator {

	private String color;
	
	public ColorShapeDecorator(AbstractShape decoratedShape, String color) {
		super(decoratedShape);
		this.color = color;
	}
	
	@Override
	public void draw() {
		super.decoratedShape.draw();
		setColor(decoratedShape, this.color);
	}
	
	private void setColor(AbstractShape shape, String color) {
		System.out.println("Color is :" + color);
	}

}
