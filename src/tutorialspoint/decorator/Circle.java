package tutorialspoint.decorator;

public class Circle extends AbstractShape {

	@Override
	public void draw() {
		System.out.println("Draw Circle");
	}

}
