package tutorialspoint.decorator;

public abstract class AbstractShape {

	public abstract void draw();
	
}
