package tutorialspoint.decorator;


/**
 * 
 * @author lukas
 * @link http://www.tutorialspoint.com/design_pattern/decorator_pattern.htm
 * @comment this is structural pattern
 */
public abstract class AbstractShapeDecorator extends AbstractShape {
	
	protected AbstractShape decoratedShape;
	
	public AbstractShapeDecorator(AbstractShape shape) {
		this.decoratedShape = shape;
	}
	
	@Override
	public void draw() {
		this.decoratedShape.draw();
	}

}
