package tutorialspoint.proxy;

public class Image implements ImageInterface {

	String filename;
	
	public Image(String filename) {
		this.filename = filename;
		loadImage(filename);
	}
	
	@Override
	public void draw() {
		System.out.println("Drawing " + this.filename);
	}
	
	private void loadImage(String filename) {
		System.out.println("Loading " + filename);
	}

}
