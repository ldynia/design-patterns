package tutorialspoint.proxy;

public class Test {

	/**
	 * @param args
	 * @link http://www.tutorialspoint.com/design_pattern/proxy_pattern.htm
	 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/proxy.html
	 * @comment it is a structural pattern
	 */
	public static void main(String[] args) {
		
		ImageInterface image = new ImageProxy("best_image.png");
		// image is loaded for the first time
		image.draw();
		// image is already loaded
		image.draw();
		
	}

}
