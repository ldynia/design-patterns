package tutorialspoint.proxy;


/**
 * 
 * @author lukas
 * @link http://www.tutorialspoint.com/design_pattern/proxy_pattern.htm
 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/proxy.html
 * @comment it is a structural pattern
 */
public class ImageProxy  implements ImageInterface {

	private Image image;
	private String filename;
	
	public ImageProxy(String filename) {
		this.filename = filename;
	}
	
	@Override
	public void draw() {
		if (image == null) {
			image = new Image(this.filename);
		}
		image.draw();
	}

}
