package tutorialspoint.proxy;

public interface ImageInterface {
	
	public void draw();
	
}
