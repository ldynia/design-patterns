package tutorialspoint.observer;


/**
 * 
 * @author lukas
 * @link http://www.tutorialspoint.com/design_pattern/observer_pattern.htm
 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/observer.html
 */
public class Teset {

	/**
	 * @param args
	 * @link http://www.tutorialspoint.com/design_pattern/observer_pattern.htm
	 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/observer.html
	 */
	public static void main(String[] args) {
		Subject subject = new Subject();

		new HexalObserver(subject);
		new OctalObserver(subject);
		new BinaryObserver(subject);

		System.out.println("First state change: 15");
		subject.setState(15);
		System.out.println("Second state change: 10");
		subject.setState(10);

	}

}
