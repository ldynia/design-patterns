package tutorialspoint.observer;

public class HexalObserver extends Observer {

	public HexalObserver(Subject subject) {
		super.subject = subject;
		super.subject.attach(this);
	}

	@Override
	public void update() {
		System.out.println("Hex String: " + Integer.toHexString(subject.getState()).toUpperCase());
	}
	
}