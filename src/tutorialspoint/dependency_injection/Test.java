package tutorialspoint.dependency_injection;

public class Test {

	/**
	 * @param args
	 * @link http://www.tutorialspoint.com/spring/spring_dependency_injection.htm
	 */
	public static void main(String[] args) {
		
		// without dependency injection
		TextEditor editorOne = new TextEditor();
		
		// with dependency injection
		TextEditor editorTwo = new TextEditor(new MachineSpellChecker());
		editorOne.getSpellChecker("test");
		editorTwo.getSpellChecker("test");

	}

}
