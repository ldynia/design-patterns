package tutorialspoint.dependency_injection;

public interface SpellCheckerInterface {
	
	public void check(String text);

}
