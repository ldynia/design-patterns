package tutorialspoint.dependency_injection;


/**
 * 
 * @author lukas
 * @link http://www.tutorialspoint.com/spring/spring_dependency_injection.htm
 */
public class TextEditor {
	
	private SpellCheckerInterface spellChecker;
	
	// This is a BAD way of calling dependencies
	// Class highly depends on a Human spell checking
	public TextEditor() {
		this.spellChecker = new HumanSpellChecker();
	}
	
	// This is GOOD way of calling dependencies.
	// Rather that creating new spell Checker - concrete dependency
	// You create generic SlepllChecker (SpellCheckerInterface)
	// And you inject it as a parameter via constructor
	public TextEditor(SpellCheckerInterface spellChecker) {
		this.spellChecker = spellChecker;
	}
	
	public void getSpellChecker(String text) {
		this.spellChecker.check(text);
	}
	

}
