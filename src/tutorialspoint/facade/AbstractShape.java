package tutorialspoint.facade;

public abstract class AbstractShape {

	public abstract void draw();
	
}
