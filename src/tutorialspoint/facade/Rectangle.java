package tutorialspoint.facade;

public class Rectangle extends AbstractShape {

	@Override
	public void draw() {
		System.out.println("I am a Rectangle");
	}

}
