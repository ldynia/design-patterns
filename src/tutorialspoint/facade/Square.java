package tutorialspoint.facade;

public class Square extends AbstractShape {

	@Override
	public void draw() {
		System.out.println("I am a Square");
	}

}
