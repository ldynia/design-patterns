package tutorialspoint.facade;


/**
 * 
 * @author lukas
 * @link http://www.tutorialspoint.com/design_pattern/facade_pattern.htm
 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/facade.html
 * @comment it is a structural pattern
 */
public class ShapeMakerFacade {

	Circle circle;
	Square square;
	Rectangle rectangle;
	
	public ShapeMakerFacade() {
		this.circle = new Circle();
		this.square = new Square();
		this.rectangle = new Rectangle();
	}
	
	public void drawCircle() {
		this.circle.draw();
	}

	public void drawSquare() {
		this.square.draw();
	}
	
	public void drawRectangle() {
		this.rectangle.draw();
	}
	
}
