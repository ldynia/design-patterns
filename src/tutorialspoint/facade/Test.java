package tutorialspoint.facade;

public class Test {

	/**
	 * @param args
	 * @link http://www.tutorialspoint.com/design_pattern/facade_pattern.htm
	 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/facade.html
	 * @comment it is a structural pattern
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ShapeMakerFacade facade = new ShapeMakerFacade();
		facade.drawCircle();
		facade.drawSquare();
		facade.drawRectangle();

	}

}
