package tutorialspoint.data_access_object;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lukas
 * @link http://www.tutorialspoint.com/design_pattern/data_access_object_pattern.htm
 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/dao.html
 * @comment It is very similar to repository
 */
public class StudentDao implements StudentDaoInterface {

	// list is working as a database
	List<Student> students;

	public StudentDao() {
		students = new ArrayList<Student>();
		Student student1 = new Student("Robert", 0);
		Student student2 = new Student("John", 1);
		students.add(student1);
		students.add(student2);
	}

	@Override
	public void deleteStudent(Student student) {
		students.remove(student.getRollNo());
		System.out.println("Student: Roll No " + student.getRollNo() + ", deleted from database");
	}

	@Override
	public List<Student> getAllStudents() {
		return students;
	}

	@Override
	public Student getStudent(int rollNo) {
		return students.get(rollNo);
	}

	@Override
	public void updateStudent(Student student) {
		students.get(student.getRollNo()).setName(student.getName());
		System.out.println("Student: Roll No " + student.getRollNo() + ", updated in the database");
	}
}