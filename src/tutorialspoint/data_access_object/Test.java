package tutorialspoint.data_access_object;

public class Test {

	/**
	 * @param args
	 * @link http://www.tutorialspoint.com/design_pattern/data_access_object_pattern.htm
	 * @link http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/dao.html
	 * @comment It is very similar to repository
	 */
	public static void main(String[] args) {
		StudentDao studentDao = new StudentDao();

		// print all students
		for (Student student : studentDao.getAllStudents()) {
			System.out.println("Student: [RollNo : " + student.getRollNo() + ", Name : " + student.getName() + " ]");
		}

		// update student
		Student student = studentDao.getAllStudents().get(0);
		student.setName("Michael");
		studentDao.updateStudent(student);

		// get the student
		studentDao.getStudent(0);
		System.out.println("Student: [RollNo : " + student.getRollNo() + ", Name : " + student.getName() + " ]");
	}
}
